﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BirthDays
{
    public partial class Form1 : Form
    {
        private string fileText; // the users text file value
        public Form1()
        {
            InitializeComponent();
            FileStream fs = File.OpenRead("Users.txt");//read the file
            byte[] b = new byte[1024];
            UTF8Encoding temp = new UTF8Encoding();
            fs.Read(b, 0, b.Length);
            fileText = temp.GetString(b);
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            if (CheckDetails())//on login check the user details
            {
                this.Hide();
                Form wnd = new Form2(textBox1.Text);
                wnd.ShowDialog();
                this.Close();
            }
            else
            {
                MessageBox.Show("Sorry wrong details"); // if the details are wrong
            }
            
        }
        /*
         * the function check if the user input details are in the user
         * database
         * input: none
         * output: true if found false otherwise
         */
        private bool CheckDetails()
        {
            bool found = false;
            string[] userDetails;
            userDetails = fileText.Split('\r','\n');
            foreach(var userDeatail in userDetails)
            {
                if(userDeatail.Split(',')[0] == textBox1.Text && userDeatail.Split(',')[1] == textBox2.Text)
                {
                    found = true;
                }
            }
            return found;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
        private void textBox1_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
        }

        private void textBox2_Click(object sender, EventArgs e)
        {
            textBox2.Text = "";
            textBox2.PasswordChar = '*'; // makes the text a password
        }
    }
}
