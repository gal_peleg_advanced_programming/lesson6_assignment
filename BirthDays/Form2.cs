﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace BirthDays
{
    public partial class Form2 : Form
    {
        private string fileText;

        public Form2()
        {
            InitializeComponent();
        }
        /*
         * this function build the form using 
         * user name parasmeter
         * input: string of the user name
         * output: none
         */
        public Form2(string userName)
        {
            InitializeComponent();
            FileStream fs = File.OpenRead(userName+"BD.txt");
            byte[] b = new byte[1024];
            UTF8Encoding temp = new UTF8Encoding();
            fs.Read(b, 0, b.Length);
            fileText = temp.GetString(b);
        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            bool found = false;
            string date = e.End.ToString(new CultureInfo("en-US")).Split(' ')[0];
            string[] details = fileText.Split('\r', '\n');
            foreach (var detail in details) //search for the selected date
            {
                if (detail.Split(',').Length > 1)
                {
                    if (detail.Split(',')[1] == date)// if found show who has birthday
                    {
                        textBox1.Text = "Today " + detail.Split(',')[0] + " has a birthday";
                        found = true;
                    }
                }
            }
            if (!found) // if the date didnt found show it
            {
                textBox1.Text = "No birthdays today";
            }
        }
    }
}
